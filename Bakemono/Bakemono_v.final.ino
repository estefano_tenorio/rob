//DEFINICAO DOS SENSORES
#define trig_direito    A0
#define echo_direito    A1
#define trig_frente     A2
#define echo_frente     A3
#define trig_esquerdo   A4
#define echo_esquerdo   A5
#define sensor_direito  2
#define sensor_frente   1
#define sensor_esquerdo 0


//DEFINICAO DE MOTORES
#define motor_a_frente 2
#define motor_b_frente 4
#define motor_frente   3

#define motor_a_traseiro 5
#define motor_b_traseiro 7
#define motor_traseiro   6

#define motor_a_esquerdo 12
#define motor_b_esquerdo 13
#define motor_esquerdo   11

#define motor_a_direito 8
#define motor_b_direito 10
#define motor_direito   9

#define motor_ccw   0
#define motor_cw    1

void setup ()
{
  Serial.begin (9600);
  // AJUSTANDO AS PORTAS DOS SENSORES
  pinMode(trig_direito, OUTPUT);
  pinMode(echo_direito, INPUT);
  
  pinMode(trig_frente, OUTPUT);
  pinMode(echo_frente, INPUT);
  
  pinMode(trig_esquerdo, OUTPUT);
  pinMode(echo_esquerdo, INPUT);
  
  //AJUSTANDO AS PORTAS DOS MOTORES
  pinMode(motor_a_frente, OUTPUT);
  pinMode(motor_b_frente, OUTPUT);
  pinMode(motor_frente, OUTPUT);
  
  pinMode(motor_a_traseiro, OUTPUT);
  pinMode(motor_b_traseiro, OUTPUT);
  pinMode(motor_traseiro, OUTPUT);

  pinMode(motor_a_direito, OUTPUT);
  pinMode(motor_b_direito, OUTPUT);
  pinMode(motor_direito, OUTPUT);
  
  pinMode(motor_a_esquerdo, OUTPUT);
  pinMode(motor_b_esquerdo, OUTPUT);
  pinMode(motor_esquerdo, OUTPUT); 
}

void loop()
{   
     
while (mede_distancia(sensor_esquerdo) > 30)
{
  gira_motor( motor_frente , motor_ccw , 245 );
  gira_motor( motor_traseiro , motor_ccw , 235 );
}
gira_motor( motor_frente , motor_ccw , 0 );
gira_motor( motor_traseiro , motor_ccw , 0 );
delay(50);  

while (mede_distancia(sensor_frente) > 32)   
{
  gira_motor( motor_esquerdo , motor_ccw , 190 );
  gira_motor( motor_direito , motor_ccw , 170 );
}
gira_motor( motor_esquerdo , motor_ccw , 0 );
gira_motor( motor_direito , motor_ccw , 0 );  
delay(50);

while (mede_distancia(sensor_direito) > 30)
{
  gira_motor( motor_frente , motor_cw , 245 );
  gira_motor( motor_traseiro , motor_cw , 235 );  
}
gira_motor( motor_frente , motor_cw , 0 );
gira_motor( motor_traseiro , motor_cw , 0 );
delay(50);

while (mede_distancia(sensor_frente) > 32)   
{
  gira_motor( motor_esquerdo , motor_ccw , 190 );
  gira_motor( motor_direito , motor_ccw , 170 );   
}
gira_motor( motor_esquerdo , motor_ccw , 0 );
gira_motor( motor_direito , motor_ccw , 0 );
delay(50);
     



/*while (mede_distancia(sensor_esquerdo) < 20 && mede_distancia(sensor_frente) < 20 && mede_distancia(sensor_direito) < 20 ) 
  {
    gira_motor( motor_frente , motor_cw , 230 );
    gira_motor( motor_traseiro , motor_ccw , 255 ); 
    gira_motor( motor_esquerdo , motor_ccw , 255 );
    gira_motor( motor_direito , motor_cw , 215 );
  
  }
      delay(100);
      gira_motor( motor_frente , motor_ccw , 0 );
      gira_motor( motor_traseiro , motor_ccw , 0 );
      gira_motor( motor_esquerdo , motor_ccw , 0 );
      gira_motor( motor_direito , motor_ccw , 0 );*/
          
    
}



void gira_motor( int motor_id, int direcao, int velocidade )
{
  int a = 0;
  int b = 0;
  // PINOS DE DIRECAO
  switch (motor_id)
  {
    case motor_frente:
      a = motor_a_frente;
      b = motor_b_frente;
    break;
      
    case motor_traseiro:
      a = motor_a_traseiro;
      b = motor_b_traseiro;
    break;
      
    case motor_direito:
      a = motor_a_direito;
      b = motor_b_direito;
    break;
    
    case motor_esquerdo:
      a = motor_a_esquerdo;
      b = motor_b_esquerdo;
    break;
  }
  
  // DIRECAO DA ROTACAO
  if(direcao == motor_ccw){
    digitalWrite( a , HIGH );
    digitalWrite( b , LOW );
  } else {
    digitalWrite( a , LOW );
    digitalWrite( b , HIGH );
  } 
  
  // MOTOR A GIRAR
  analogWrite ( motor_id , velocidade );
  
}

float mede_distancia(int sensor_id)
{
  float Temp, Dist;
  int trig_sensor = 0;
  int echo_sensor = 0;
  String str_sensor = "";
  
  switch(sensor_id){
   
    case sensor_direito:
      trig_sensor = trig_direito;  
      echo_sensor = echo_direito;
      str_sensor = "Dir";
    break;
    
    case sensor_frente:
      trig_sensor = trig_frente; 
      echo_sensor = echo_frente;
      str_sensor = "Fre";   
    break;
    
    case sensor_esquerdo:
      trig_sensor = trig_esquerdo;
      echo_sensor = echo_esquerdo;
      str_sensor = "Esq";    
    break;
  }
  
  digitalWrite(trig_sensor, LOW);
  delayMicroseconds(2);
  digitalWrite(trig_sensor, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig_sensor, LOW);
  
  Temp = pulseIn(echo_sensor, HIGH);
  Dist = (Temp/2) /29.1;
  
 Serial.print(str_sensor);
  Serial.print(" ");
  Serial.print(Dist);
  Serial.print("cm");
  Serial.println(); 
  return Dist;
}



