
 
#include <AFMotor.h>
#include <Ultrasonic.h>

// shield Pinos utilizados para controle de motores DC : Pinos 11, 3, 5 e 6

AF_DCMotor M1(1); //Seleciona o motor 1
AF_DCMotor M2(2);
AF_DCMotor M3(3);
AF_DCMotor M4(4);
const int vccPin = 9; //jumper vermelho
const int trigPin = 8; //jumper azul
const int echoPin = 7; //jumper amarelo
const int gndPin = 6; // jumper verde
const int vccPin2 = 5; // jumper vermelho
const int trigPin2 = 4; //jumper azul
const int echoPin2 = 3; //jumper amarelo 
const int gndPin2 = 2; //jumper vermelho
const int vccPin3 = 13; //jumper vermelho
const int trigPin3 = 12; //jumper azul
const int echoPin3 = 11; //jumper amarelo
const int gndPin3 = 10; // jumper verde



//Ultrasonic ultrasonic1(12,11);
//Ultrasonic ultrasonic2(8,7);
//Ultrasonic ultrasonic3(4,3);
 
void setup()
{
//Serial.begin(9600); 
//pinMode (vccPin,OUTPUT);
//pinMode (gndPin,OUTPUT); 
//pinMode (trigPin,OUTPUT);
//pinMode (echoPin,INPUT);
//pinMode (vccPin2,OUTPUT);
//pinMode (gndPin2,OUTPUT); 
//pinMode (trigPin2,OUTPUT);
//pinMode (echoPin2,INPUT);
//pinMode (vccPin3,OUTPUT);
//pinMode (gndPin3,OUTPUT); 
//pinMode (trigPin3,OUTPUT);
//pinMode (echoPin3,INPUT);

//Serial.println("Lendo dados do sensor...");

  }
 
void loop()
{
  // digitalWrite(vccPin, HIGH);
  // digitalWrite(trigPin,LOW);
  // delayMicroseconds(2);
  // digitalWrite(trigPin,HIGH);
   //delayMicroseconds(10);
   //digitalWrite(trigPin,LOW);
  // unsigned long duracao = pulseIn(echoPin,HIGH);
  // int distancia = duracao / 58;
  // Serial.print("Distancia em cm: ");
 //  Serial.println(distancia);
 //  delay(1000);

  // digitalWrite(vccPin2, HIGH);
  // digitalWrite(trigPin2,LOW);
  // delayMicroseconds(2);
  // digitalWrite(trigPin2,HIGH);
  // delayMicroseconds(10);
  // digitalWrite(trigPin2,LOW);
  // unsigned long duracao2 = pulseIn(echoPin2,HIGH);
 //  int distancia2 = duracao2 / 58;
 //  Serial.print("Distancia em cm: ");
  // Serial.println(distancia2);
  // delay(1000);
   
 // digitalWrite(vccPin3, HIGH);
 // digitalWrite(trigPin3,LOW);
 // delayMicroseconds(2);
 // digitalWrite(trigPin3,HIGH);
 // delayMicroseconds(10);
 // digitalWrite(trigPin3,LOW);
  //unsigned long duracao3 = pulseIn(echoPin3,HIGH);
 // int distancia3 = duracao3 / 58;
 // Serial.print("Distancia em cm: ");
 // Serial.println(distancia3);
 // delay(1000);

  
//Le as informacoes do sensor, em cm e pol
//float cmMsec;
//long microsec = ultrasonic3.timing();
//cmMsec = ultrasonic3.convert(microsec, Ultrasonic::CM);
//Exibe informacoes no serial monitor
//Serial.print("Distancia em cm: ");
//Serial.print(cmMsec);
//delay(1000);
  

  
  


delay(1000);
M1.setSpeed(255); //Define a velocidade maxima
M1.run(FORWARD); //Gira o motor sentido horario
M2.setSpeed(255);
M2.run(FORWARD);
M3.setSpeed(255);
M3.run(FORWARD);
M4.setSpeed(255); //Define a velocidade maxima
M4.run(FORWARD); //Gira o motor sentido horario

delay(1000); 


M1.setSpeed(0);
M1.run(RELEASE); //Desliga o motor
M2.setSpeed(0);
M2.run(RELEASE);
M3.setSpeed(0);
M3.run(RELEASE);
M4.setSpeed(0);
M4.run(RELEASE); //Desliga o motor

 
delay(1000);
M1.setSpeed(100); //Define velocidade baixa
M1.run(BACKWARD); //Gira o motor sentido anti-horario
M2.setSpeed(100);
M2.run(BACKWARD);
M3.setSpeed(100);
M3.run(BACKWARD);
M4.setSpeed(100); //Define velocidade baixa
M4.run(BACKWARD); //Gira o motor sentido anti-horario
 


delay(1000);
M1.setSpeed(0);
M1.run(RELEASE); //Desliga o motor
M2.setSpeed(0);
M2.run(RELEASE);
M3.setSpeed(0);
M3.run(RELEASE);
M4.setSpeed(0);
M4.run(RELEASE);


delay(1000); //Aguarda 5 segundos e repete o processo
}
