#include <AFMotor.h>
#include <Ultrasonic.h>

// shield Pinos utilizados para controle de motores DC : Pinos 11, 3, 5 e 6
//define os motores
AF_DCMotor M1(1); //Seleciona o motor da esquerda
AF_DCMotor M2(2); //seleciona o motor traseiro
AF_DCMotor M3(3); // seleciona o motor da direita
AF_DCMotor M4(4); //seleciona o motor de frente

//define os sensores
const int vccPin = 2; //jumper vermelho
const int trigPin = 12; //jumper azul
const int echoPin = 10; //jumper amarelo


const int vccPin2 = A5; // jumper vermelho
const int trigPin2 = A4; //jumper azul
const int echoPin2 = A3; //jumper amarelo 
const int gndPin2 = A2; //jumper vermelho


const int vccPin3 = 4; //jumper vermelho
const int trigPin3 = A1; //jumper azul
const int echoPin3 = A0; //jumper amarelo




Ultrasonic ultrasonic1(12,10); //sensor frontal
Ultrasonic ultrasonic2(A4,A3); // sensor da esquerda
Ultrasonic ultrasonic3(A1,A0); //sensor da direita 
 

void setup() {
  // put your setup code here, to run once:
 
pinMode (vccPin,OUTPUT); //define o pino com saida,  
pinMode (trigPin,OUTPUT);
pinMode (echoPin,INPUT); //define o pino com entrada
pinMode (vccPin2,OUTPUT);
pinMode (gndPin2,OUTPUT); 
pinMode (trigPin2,OUTPUT);
pinMode (echoPin2,INPUT);
pinMode (vccPin3,OUTPUT);
pinMode (trigPin3,OUTPUT);
pinMode (echoPin3,INPUT);


}

void loop()
{
  digitalWrite(vccPin, HIGH);
  digitalWrite(vccPin2, HIGH);
  digitalWrite(vccPin3, HIGH);

  

while(sensorFrente() > 30) //sensor frontal
{
  M1.setSpeed(255);
  M1.run(FORWARD);
  M3.setSpeed(255);
  M3.run(FORWARD); 
}
M1.setSpeed(0);
M1.run(RELEASE);
M3.setSpeed(0);
M3.run(RELEASE);
delay(50);  

while (sensorDireita() > 30)  //Sensor direita
{
  M4.setSpeed(255);
  M4.run(FORWARD);
  M2.setSpeed(255);
  M2.run(FORWARD); 
}
M4.setSpeed(0);
M4.run(RELEASE);
M2.setSpeed(0);
M2.run(RELEASE);
delay(50);

while (sensorEsquerda() > 30)//sensor esquerda
{
  M4.setSpeed(255);
  M4.run(BACKWARD);
  M2.setSpeed(255);
  M2.run(BACKWARD); 
}
M4.setSpeed(0);
M4.run(RELEASE);
M2.setSpeed(0);
M2.run(RELEASE);
delay(50);

while (sensorFrente() > 30) //sensor frente   
{
  M1.setSpeed(255);
  M1.run(FORWARD);
  M3.setSpeed(255);
  M3.run(FORWARD); 
}

M1.setSpeed(0);
M1.run(RELEASE);
M3.setSpeed(0);
M3.run(RELEASE);
delay(50);
}

float sensorFrente(){
  
float cmMsec;
long microsec = ultrasonic1.timing();
cmMsec = ultrasonic1.convert(microsec, Ultrasonic::CM);
//Exibe informacoes no serial monitor
Serial.begin(9600);
Serial.println("Distancia em cm frente: ");
Serial.println(cmMsec);
delay(500);
return cmMsec;
}


float sensorDireita(){  

float cmMsec2;
long microsec2 = ultrasonic2.timing();
cmMsec2 = ultrasonic2.convert(microsec2, Ultrasonic::CM);
//Exibe informacoes no serial monitor
Serial.begin(9600);
Serial.println("Distancia em cm direita: ");
Serial.println(cmMsec2);
delay(500);
return cmMsec2;
}


float sensorEsquerda(){
  
float cmMsec3;
long microsec3 = ultrasonic3.timing();
cmMsec3 = ultrasonic3.convert(microsec3, Ultrasonic::CM);
//Exibe informacoes no serial monitor
Serial.begin(9600);
Serial.println("Distancia em cm esquerda: ");
Serial.println(cmMsec3);
delay(500);
return cmMsec3;
}

